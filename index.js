var through = require('through2'),
  fs = require('fs');
// var util = require('util');
var debug = require('debug')('gulp-jscad-files');
var verbose = require('debug')('gulp-jscad-files:verbose');
var silly = require('debug')('gulp-jscad-files:silly');
var jscadFilesPackage = require('./package.json');
var { getPackage, getJscadFiles } = require('./getPackage');

debug('gulp-jscad-files', jscadFilesPackage.version);

/**
 * Gets the jscad dependent libraries from a gulp stream.  Assumes the stream
 * is a package.json file of a jscad project.
 * @param {*} file
 * @param {*} stream
 * @param {*} loaded
 * @param {*} depth
 */
function getPackageStream(file, stream, loaded, depth) {
  depth = depth || 0;

  verbose('getPackage', file, depth);

  // if (depth > 1) {
  //   throw newError(
  //     'GULP_JSCAD_FILES_PACKAGE_DEPTH_EXCEEDED',
  //     'getPackage depth exceeded',
  //     { depth, file }
  //   );
  // }

  silly('getPackage contents', file.path, file.contents.toString());
  var pkg = JSON.parse(file.contents.toString());
  return getPackage(pkg);
}

module.exports = function() {
  return through.obj(function(file, encoding, callback) {
    var self = this;

    getPackageStream(file, self, {}, 0)
      .map(f => {
        verbose('exports getPackage.map file:', f);
        return f;
      })
      .filter(f => {
        return fs.existsSync(`node_modules/${f}/jscad.json`);
      })
      .forEach(jscadlib => {
        getJscadFiles(jscadlib).forEach(libfile => {
          silly('libfile', libfile);
          self.push(libfile);
        });
      });

    // console.warn('cb');
    callback();
  });
};
