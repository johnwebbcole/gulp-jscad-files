## 4.1.0 (2019-09-18)

* filter out optionalDependencies and do not include them in the injected files ([ccdeea9](https://gitlab.com/johnwebbcole/gulp-jscad-files/commit/ccdeea9))
* update readme with getInjected info ([313512f](https://gitlab.com/johnwebbcole/gulp-jscad-files/commit/313512f))



## 4.0.0 (2019-09-15)

* 4.0.0 ([1d271ca](https://gitlab.com/johnwebbcole/gulp-jscad-files/commit/1d271ca))
* adding launch for vscode to debug test ([0082d33](https://gitlab.com/johnwebbcole/gulp-jscad-files/commit/0082d33))
* Refactor to add a check for optional dependencies ([9f1b303](https://gitlab.com/johnwebbcole/gulp-jscad-files/commit/9f1b303))



## <small>3.0.1 (2018-12-24)</small>

* 3.0.1 ([5081f7c](https://gitlab.com/johnwebbcole/gulp-jscad-files/commit/5081f7c))
* remove gulp-util ([5f6d2d0](https://gitlab.com/johnwebbcole/gulp-jscad-files/commit/5f6d2d0))



## 3.0.0 (2018-12-24)

* 3.0.0 ([6c7c0c6](https://gitlab.com/johnwebbcole/gulp-jscad-files/commit/6c7c0c6))
* add cicd ([7aefef6](https://gitlab.com/johnwebbcole/gulp-jscad-files/commit/7aefef6))
* add missing dependency ([7f955a5](https://gitlab.com/johnwebbcole/gulp-jscad-files/commit/7f955a5))
* don't use yarn ([395fac1](https://gitlab.com/johnwebbcole/gulp-jscad-files/commit/395fac1))
* fix cicd ([63ca3c5](https://gitlab.com/johnwebbcole/gulp-jscad-files/commit/63ca3c5))
* test ([1e16433](https://gitlab.com/johnwebbcole/gulp-jscad-files/commit/1e16433))
* Updated to search package dependencies and create ([5cc0c6b](https://gitlab.com/johnwebbcole/gulp-jscad-files/commit/5cc0c6b))



## <small>2.0.1 (2017-01-01)</small>

* 2.0.1 ([87fcfd6](https://gitlab.com/johnwebbcole/gulp-jscad-files/commit/87fcfd6))



## 2.0.0 (2016-12-29)

* 2.0.0 ([4100ea2](https://gitlab.com/johnwebbcole/gulp-jscad-files/commit/4100ea2))
* commenting out tests ([5ff8c06](https://gitlab.com/johnwebbcole/gulp-jscad-files/commit/5ff8c06))
* modified to scan the package.json file for dependencies ([80e341b](https://gitlab.com/johnwebbcole/gulp-jscad-files/commit/80e341b))



## <small>1.0.3 (2016-07-13)</small>

* 1.0.3 ([051a6c6](https://gitlab.com/johnwebbcole/gulp-jscad-files/commit/051a6c6))
* added check for files being relative to current directory ([ad18bfd](https://gitlab.com/johnwebbcole/gulp-jscad-files/commit/ad18bfd))



## <small>1.0.2 (2016-07-13)</small>

* 1.0.2 ([2a2933e](https://gitlab.com/johnwebbcole/gulp-jscad-files/commit/2a2933e))
* check for pkg before getting dependencies ([1dfa77e](https://gitlab.com/johnwebbcole/gulp-jscad-files/commit/1dfa77e))
* If the main file is `index.js` rename it to the package name. ([0ad566b](https://gitlab.com/johnwebbcole/gulp-jscad-files/commit/0ad566b))
* inital commit ([1789504](https://gitlab.com/johnwebbcole/gulp-jscad-files/commit/1789504))
* Initial commit ([77e5aea](https://gitlab.com/johnwebbcole/gulp-jscad-files/commit/77e5aea))
* updated packge.json ([03393ed](https://gitlab.com/johnwebbcole/gulp-jscad-files/commit/03393ed))



