var util = require('util');
var fs = require('fs');
var path = require('path');
const File = require('vinyl');
var debug = require('debug')('gulp-jscad-files:getPackage');
var verbose = require('debug')('gulp-jscad-files:getPackage:verbose');
var silly = require('debug')('gulp-jscad-files:getPackage:silly');

function newError(name, message, object) {
  var err = new Error(
    `${message}: ${
      object ? util.inspect(object, { depth: 1, colors: true }) : ''
    }`
  );

  err.name = name;

  return err;
}

/**
 * Opens a package.json file and returns the `dependencies`
 * as an array.  Does not look at the version information.
 * @param {string} packagePath The path to the package.json file to load.
 */
function getDependencies(packagePath) {
  silly('getDependencies', packagePath);
  var packageJson = JSON.parse(fs.readFileSync(packagePath).toString());
  var { name, dependencies, optionalDependencies } = Object.assign(
    { dependencies: {}, optionalDependencies: {} },
    packageJson
  );
  var dependenciesWithoutOptional = Object.keys(dependencies || {}).filter(
    d => !optionalDependencies[d]
  );
  silly('getDependencies', packagePath, {
    name,
    dependencies,
    optionalDependencies,
    dependenciesWithoutOptional
  });

  return {
    dependencies: dependenciesWithoutOptional,
    optionalDependencies: Object.keys(optionalDependencies || {})
  };
}

/**
 * Recursivly returns all dependencies for a given package name.
 * @param {string} p  package name
 */
function getAllDependencies(p, pkg) {
  verbose('getAllDependencies', p);
  var files = [];
  var cache = {};
  var optionalLogger = require('debug')('gulp-jscad-files:getPackage:optional');

  function findDependenciesRecursive(name, pkg, optional = false) {
    var packagePath = `node_modules/${name}/package.json`;
    var logger = optional ? optionalLogger : verbose;
    logger('** findDependenciesRecursive', name, optional, packagePath);

    if (!files.includes(name)) {
      if (fs.existsSync(packagePath)) {
        logger('getAllDependencies package exsits', name);
        files.push(name);

        var d = getDependencies(packagePath);
        logger('getAllDependencies deps', name, d);
        cache[name] = d;
        d.dependencies.forEach(dependentPackageName => {
          var isOptional =
            d.optionalDependencies &&
            d.optionalDependencies.includes(dependentPackageName);
          logger(
            'getAllDependencies forEach dependency',
            name,
            dependentPackageName,
            isOptional,
            pkg.optionalDependencies
          );
          if (!isOptional)
            findDependenciesRecursive(
              dependentPackageName,
              d,
              optional || isOptional
            );
        });
      } else {
        debug('getAllDependencies package does not exist', {
          name,
          optional,
          pkg
        });

        if (optional) {
          debug(`${name} is an optional dependency`);
        } else {
          var { name, dependencies, optionalDependencies } = pkg;
          silly('not found', p, optional, {
            name,
            dependencies,
            optionalDependencies
          });
          throw newError(
            'GULP_JSCAD_FILES_PACKAGE_NOTFOUND',
            `The package "${name}" does not exist`,
            pkg
          );
        }
      }
    }
  }

  findDependenciesRecursive(p, pkg);

  verbose('getAllDependencies return', { files, cache });
  return { files, cache };
}

/**
 * Flattens an array of {files, cache} objects into a single
 * {files, cache} object.
 * @param {array} d
 */
function flatten(d) {
  silly('flatten', d);
  return d.reduce(
    (acc, v) => {
      v.files.forEach(name => {
        if (!acc.files.includes(name)) acc.files.push(name);
      });

      Object.entries(v.cache).forEach(([key, value]) => {
        if (!acc.cache[key]) acc.cache[key] = value;
      });
      return acc;
    },
    { files: [], cache: {} }
  );
}

/**
 * Sorts the list of library files in dependency order.
 * If a circular dependency is found, it throws an error.
 * @param {object} param
 * @param {array} param.files
 * @param {object} param.cache
 */
function sort({ files, cache }) {
  silly('sort', files, cache);
  var dependencies = [];
  var recheck = [];
  var remaining = files.map(f => f); // copy files
  verbose('sort remaining', remaining);
  var rlength = remaining.length;
  var passcount = 0;

  while (remaining.length > 0 && passcount < 10) {
    silly('** while remaining.length > 0', remaining.length, passcount);
    passcount++;

    /**
     * Loop through each remaining dependency.  The remaining
     * list is regenerated on each pass of the while loop.
     */
    remaining.forEach(name => {
      /**
       * push libraries with no dependencies on the front
       */
      if (cache[name].dependencies.length == 0) {
        dependencies.unshift(name);
      } else {
        /**
         * find the highest index of a libraries dependency
         */
        var indexes = cache[name].dependencies.map(lib => {
          var idx = dependencies.indexOf(lib);
          silly('sort lib', name, lib, idx);
          return idx;
        });
        silly('sort indexes', name, indexes);
        if (indexes.includes(-1)) {
          // some libraries were not found yet, recheck
          recheck.unshift(name);
        } else {
          silly('sort all indexes found', name, indexes);
          dependencies.push(name);
        }
      }
    });

    remaining = files.filter(name => !dependencies.includes(name));

    if (remaining.length == rlength) {
      debug('sort Circular dependencies found', rlength, remaining, passcount);
      throw newError(
        'GULP_JSCAD_FILES_CIRCULAR_DEPENDENCIES',
        'Possible circular dependencies',
        { remaining, dependencies }
      );
    } else {
      silly('sort new remaining', remaining.length, rlength);
      rlength = remaining.length;
    }
    silly('sort remaining', remaining);
  }
  return dependencies;
}

/**
 * Gets the jscad dependent libraries from a gulp stream.  Assumes the stream
 * is a package.json file of a jscad project.
 * @param {Object} pkg A package.json object.
 */
function getPackage(pkg) {
  verbose('getPackage', pkg);
  
  /**
   * make sure each package has both dependencies and optional dependencies.
   * This makes it easier to deal with odd package files that don't.
   */
  pkg = Object.assign({dependencies:{}, optionalDependencies: {}}, pkg);

  var dependencyFiles = Object.keys(pkg.dependencies).filter(d=> !pkg.optionalDependencies[d]).map(p => {
    silly('getPackage p', p);
    var d = getAllDependencies(p, pkg);
    // debug('getPackage p', { p, d });
    return d;
  });

  // debug('getPackage dependencyFiles', dependencyFiles);
  var flat = flatten(dependencyFiles);
  // debug('getPackage flatten', flat);

  var sorted = sort(flat);
  silly('getPackage sort', sorted);
  return sorted;
}

/**
 * Returns an array of gulp file objects from a given jscad library
 * name.  Assumes that there is a `jscad.json` file with a `files`
 * list of dependent files.
 * @param {string} jscadlib name of a node_modules jscad library
 */
function getJscadFiles(jscadlib) {
  var jscadJsonPath = `node_modules/${jscadlib}/jscad.json`;

  debug('getJscadFiles', jscadlib, jscadJsonPath);

  var jscad = JSON.parse(fs.readFileSync(jscadJsonPath));

  var files = Array.isArray(jscad.files) ? jscad.files : [jscad.files];

  return files.map(filename => {
    var deppath = path.resolve('node_modules/' + jscadlib, filename);
    silly('getJscadFiles filename', { filename, deppath });
    var depfile = new File({
      path: deppath,
      contents: fs.readFileSync(deppath)
    });
    silly('getJscadFiles depfile', { filename, depfile });
    return depfile;
  });
}

function getInjected(pkg) {
  return getPackage(pkg)
    .filter(f => {
      return fs.existsSync(`node_modules/${f}/jscad.json`);
    })
    .reduce((injected, jscadlib) => {
      var jscadJsonPath = `node_modules/${jscadlib}/jscad.json`;
      var jscad = JSON.parse(fs.readFileSync(jscadJsonPath).toString());

      var files = Array.isArray(jscad.files) ? jscad.files : [jscad.files];

      files.forEach(jscadfile =>
        injected.push(path.resolve('node_modules/' + jscadlib, jscadfile))
      );
      return injected;
    }, []);
}

module.exports = { getPackage, getJscadFiles, getInjected };
